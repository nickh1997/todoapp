package edu.towson.cosc435.HOMAYOUNI.todos

import android.content.res.Configuration
import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.clickable
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Menu
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import edu.towson.cosc435.HOMAYOUNI.todos.ui.theme.Todos_NICHOLASHOMAYOUNITheme
import kotlinx.coroutines.launch

class MainActivity : ComponentActivity() {
    @ExperimentalFoundationApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Todos_NICHOLASHOMAYOUNITheme {
                Surface(color = MaterialTheme.colors.background) {
                    val nav = rememberNavController()
                    //val vm: TodoViewModel by viewModels()
                    val drawerState = rememberDrawerState(DrawerValue.Closed)
                    val scaffoldState = rememberScaffoldState()
                    val scope = rememberCoroutineScope()
                    val openDrawer = {
                        scope.launch {
                            drawerState.open()
                        }
                    }
                    Scaffold(
                        topBar = {
                            TopAppBar (
                                title = { Text("Todo") },
                                navigationIcon = {
                                    IconButton(onClick = {
                                        scope.launch {
                                            scaffoldState.drawerState.open()
                                        }
                                    }) {
                                        Icon(Icons.Filled.Menu, "")
                                    }
                                }
                            )
                        },
                        scaffoldState = scaffoldState,
                        drawerContent = {
                            // Drawer content
                            Drawer(
                                onDestinationClicked = { route ->
                                    scope.launch {
                                        scaffoldState.drawerState.close()
                                    }
                                    nav.navigate(route) {
                                        popUpTo = nav.graph.startDestinationId
                                        launchSingleTop = true
                                    }
                                }
                            )
                        },
                        floatingActionButton = {
                            ExtendedFloatingActionButton(
                                text = { Text("Add Todo") },
                                onClick = {
                                    scope.launch {
                                        scaffoldState.drawerState.apply {
                                            if (isClosed) open() else close()
                                        }
                                    }
                                }
                            )
                        },
                        isFloatingActionButtonDocked = true,
                    ) {
                        NavHost(
                            navController = nav,
                            startDestination = NewTodoScreen.Home.route
                        ) {
                            composable(NewTodoScreen.Home.route) {
                                Home(
                                    openDrawer = {
                                        openDrawer()
                                    }
                                )
                            }
                            composable(NewTodoScreen.NewTodo.route) {
                                NewTodo(
                                    navController = nav
                                )
                            }
                        }
                        // Screen content
                        Card(
                            shape = RoundedCornerShape(5.dp),
                            elevation = 16.dp,
                            modifier = Modifier
                                .padding(start = 16.dp, end = 16.dp, top = 5.dp, bottom = 5.dp)
                                .fillMaxWidth()
                        ) {
                            LazyColumn(
                                verticalArrangement = Arrangement.spacedBy(4.dp),
                                contentPadding = PaddingValues(horizontal = 16.dp, vertical = 8.dp)
                            ) {
                                items(50) {
                                    Text(
                                        text = "TodoTitle $it",
                                        fontSize = 24.sp,
                                        fontWeight = FontWeight.Bold,
                                        //        textAlign = TextAlign.Center,
                                        modifier = Modifier
                                            .fillMaxWidth()
                                            .padding(vertical = 24.dp)
                                    )
                                }
                            }
                        }
                    }

                }
            }
        }
    }
}


@Composable
fun Home(openDrawer: () -> Unit) {                  // Home is first screen
    Column(modifier = Modifier.fillMaxSize()) {
        Column(
            modifier = Modifier.fillMaxSize(),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally) {
        }
    }
}

@Composable
fun NewTodo(navController: NavController) {
    Column(modifier = Modifier.fillMaxSize()) {
        Column(
            modifier = Modifier.fillMaxSize(),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally) {
            Text(text = "add new todo.", style = MaterialTheme.typography.h4)
        }
    }
}

@Composable
fun Drawer(
    modifier: Modifier = Modifier,
    onDestinationClicked: (route: String) -> Unit
) {
    Column(
        modifier
            .fillMaxSize()
            .padding(start = 24.dp, top = 48.dp)
    ) {
        screen.forEach { screen ->
            Spacer(Modifier.height(24.dp))
            Text(
                text = screen.title,
                modifier = Modifier.clickable {
                    onDestinationClicked(screen.route)
                }
            )
        }
    }
}


private val screen = listOf(
    NewTodoScreen.Home,
    NewTodoScreen.NewTodo
)

sealed class NewTodoScreen(val title: String, val route: String) {
    object Home : NewTodoScreen("Home", "home")
    object NewTodo : NewTodoScreen("NewTodo", "newTodo")
}





