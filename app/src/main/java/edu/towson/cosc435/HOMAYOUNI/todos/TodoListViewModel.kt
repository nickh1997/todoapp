package edu.towson.cosc435.HOMAYOUNI.todos

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel

class TodoListViewModel : ViewModel() {
    private val _todos: MutableState<List<Todo>> = mutableStateOf(listOf())
    val todos: State<List<Todo>> = _todos
    private val _selected: MutableState<Todo>
    val selectedTodo: State<Todo>

    private val _repository: ITodosRepository = TodosRepository()

    init {
        _todos.value = _repository.getTodos()
        _selected = mutableStateOf(_todos.value.get(0))
        selectedTodo = _selected
    }

    fun addTodo(todo: Todo) {
        _repository.addTodo(todo)
        _todos.value = _repository.getTodos()
    }

    fun deleteTodo(idx: Int) {
        _repository.deleteTodo(idx)
        _todos.value = _repository.getTodos()
    }

    fun toggleComplete(idx: Int) {
        _repository.toggleComplete(idx)
        _todos.value = _repository.getTodos()
    }

    fun selectTodo(todo: Todo) {
        _selected.value = todo
    }
}
