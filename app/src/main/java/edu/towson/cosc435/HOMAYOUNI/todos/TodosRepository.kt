package edu.towson.cosc435.HOMAYOUNI.todos

class TodosRepository : ITodosRepository {
    private var _todos: List<Todo>
    private val _original: List<Todo>

    init {
        _original = (0..10).map { i ->
            Todo("TodoTitle $i", "TodoComment $i", i,  false)
        }
        _todos = _original.map { s -> s}
    }

    override fun getTodos(): List<Todo> {
        return _todos
    }

    override fun deleteTodo(idx: Int) {
        _todos = _todos.subList(0, idx) + _todos.subList(idx+1, _todos.size)
    }

    override fun addTodo(todo: Todo) {
        _todos = listOf(todo) + _todos
    }

    override fun toggleComplete(idx: Int) {
        val todo = _todos.get(idx)
        val newTodo = todo.copy(isComplete = !todo.isComplete)
        _todos = _todos.subList(0, idx) + listOf(newTodo) + _todos.subList(idx+1, _todos.size)
    }

}
