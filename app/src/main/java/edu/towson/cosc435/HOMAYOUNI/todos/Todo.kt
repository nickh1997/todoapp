package edu.towson.cosc435.HOMAYOUNI.todos

data class Todo(
    val title: String,
    val comments: String,
    val dueDate: Int,
    val isComplete: Boolean
    ) {
}
