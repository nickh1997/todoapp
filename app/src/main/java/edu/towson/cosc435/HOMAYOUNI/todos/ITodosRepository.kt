package edu.towson.cosc435.HOMAYOUNI.todos

interface ITodosRepository {
    fun getTodos(): List<Todo>
    fun deleteTodo(idx: Int)
    fun addTodo(todo: Todo)
    fun toggleComplete(idx: Int)
}
